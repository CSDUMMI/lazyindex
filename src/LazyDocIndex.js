'use strict'

class LazyDocIndex {
  constructor () {
    this._index = {}
  }

  get (key, fullOp = false) {
    if(fullOp) {
      return this._get(key)
    } else if(this._get(key)) {
      return this._get(key).payload.value
    } else {
      return null
    }
  }

  _get(key) {
    for (let entry of this._index) {
      switch (entry.op) {
        case "PUTALL":
          let docs = entry.payload.docs.collect()
          if(docs.indexOf(key) != -1) {
            return entry.payload.docs[docs.indexOf(key)]
          }
          break
        case "PUT":
          if(key == entry.payload.key) {
            return entry
          }
          break
        case "DEL":
          if(key == entry.payload.key) {
            return undefined
          }
          break
        default:
          return undefined
      }
    }
  }

  updateIndex (oplog) {
    this._index = oplog.values.slice()
  }
}

module.exports = LazyDocIndex
