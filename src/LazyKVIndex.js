"use strict";

class LazyKVIndex {
  constructor() {
    this._index = {
      values: [],
      state: {}
    }
  }

  get(key) {
    if(this._index.state[key] !== undefined) {
      return this._index.state[key] === null ? undefined : this._index.state[key]
    } else {
      for (let item of this._index.values) {
        if(key == item.payload.key) {
          if(item.payload.op == "PUT") {
            this._index.state[key] = item.payload.value
            return this._index.state[key]
          } else if(item.payload.op == "DEL") {
            this._index.state[key] = null
            return undefined
          }
        }
      }
    }
  }

  updateIndex(oplog) {
    this._index.state = {}
    this._index.values = oplog.values.slice().reverse()
  }
}

module.exports = LazyKVIndex
