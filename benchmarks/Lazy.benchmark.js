const benchmarkIndex = require("./benchmarkIndex")
const LazyIndex = require("../src/LazyKVIndex")

async function benchmark(benchmarker) {
  let name = "LazyIndex"
  benchmarker.setBenchmarkName(name)

  await benchmarkIndex(benchmarker, name, LazyIndex)
}

module.exports = { benchmark };
