const benchmarkIndex = require("./benchmarkIndex")

async function benchmark(benchmarker) {
  let name = "StrictIndex"
  benchmarker.setBenchmarkName(name)

  await benchmarkIndex(benchmarker, name)
}

module.exports = { benchmark };
