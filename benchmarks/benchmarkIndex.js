const IPFS = require("ipfs")
const OrbitDB = require("orbit-db")

async function benchmarkIndex(benchmarker, name, index) {
  const ipfs = await IPFS.create({
      repo: benchmarker.dir + "/jsipfs",
      offline: true, // Don't waste time with connections
      silent: true
    })
  const orbitdb = await OrbitDB.createInstance(ipfs)
  const options = index !== undefined ? { Index: index } : {}
  const db = await orbitdb.keyvalue(name, options)

  // Generating data (not to be included in the benchmarks)
  const height = 5*10**5
  let data = {}

  for(let i = 0;i < height;i++) {
    data[Date.now()] = Math.random()
  }

  let keys = Object.keys(data)


  // Benchmark setup
  let reads = 0
  let readCycles = 0
  let writes = 0
  let writeCycles = 0

  benchmarker.trackMemory()
  benchmarker.trackCpu()

  benchmarker.addMetric({
    name: "writes",
    get: () => writes
  })
  benchmarker.addMetric({
    name: "writes per second",
    get: () => {
      const perSecond = writes - writeCycles
      writeCycles = writes
      return perSecond
    }
  })

  benchmarker.addMetric({
    name: "reads",
    get: () => reads
  })
  benchmarker.addMetric({
    name: "reads per second",
    get: () => {
      const perSecond = reads - readCycles
      readCycles = reads
      return perSecond
    }
  })

  // Benchmarking
  benchmarker.log(`Add data to ${name}`)
  benchmarker.startRecording()

  for (let key of keys) {
    await db.put(key, data[key])
    writes += 1
  }

  for(let key of keys) {
    db.get(key)
    reads += 1
  }

  benchmarker.stopRecording()
  ipfs.stop()
}

module.exports = benchmarkIndex;
