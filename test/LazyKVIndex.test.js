const LazyKVIndex = require("../src/LazyKVIndex")
const IPFS = require("ipfs")
const OrbitDB = require("orbit-db")

jest.setTimeout(10000)
let ipfs, orbitdb, db

beforeAll(async () => {
  ipfs = await IPFS.create({ silent: true,
                             repo: "./jsipfs" + Math.random().toString(),
                             offline: true
                           })
  orbitdb = await OrbitDB.createInstance(ipfs)
})

test("Create db", async () => {
  db = await orbitdb.keyvalue("dbTest", { Index: LazyKVIndex })
  expect(db.dbname).toBe("dbTest")
})

test("Adding data and reading it", async () => {
  for (var i = 0; i < 10**3; i++) {
    let key = Math.floor(Math.random() * 10) + 1
    let value = Date.now()

    await db.put(key, value)
    expect(await db.get(key)).toBe(value)
  }
})

afterAll(async () => {
  await ipfs.stop()
})
